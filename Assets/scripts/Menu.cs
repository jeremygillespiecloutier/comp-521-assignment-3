﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/*
 * Jeremy Gillespie-Cloutier (260688666)
 * This class manages the UI and the reporting of simulation statistics.
 */
public class Menu : MonoBehaviour {

    Toggle menuToggle, pauseToggle;
    Button calculateButton, restartButton;
    Game game;
    bool calculate = false;
    public int rounds = 0, failures = 0, length = 0, moves = 0;

	// Use this for initialization
	void Start () {
        game = GameObject.Find("Level").GetComponent<Game>();
        menuToggle=GameObject.Find("MenuToggle").GetComponent<Toggle>();
        menuToggle.onValueChanged.AddListener((selected) => { pauseToggle.gameObject.SetActive(selected); });
        pauseToggle = GameObject.Find("PauseToggle").GetComponent<Toggle>();
        pauseToggle.onValueChanged.AddListener((selected) => { game.paused = selected;});
        pauseToggle.gameObject.SetActive(false);
        calculateButton = GameObject.Find("CalculateButton").GetComponent<Button>();
        calculateButton.onClick.AddListener(() => { calculate = true; });
        restartButton = GameObject.Find("RestartButton").GetComponent<Button>();
        restartButton.onClick.AddListener(() => {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        });
	}

    void Update()
    {
        if (calculate)
        {
            calculate = false;
            if(moves>0)
                Debug.Log("Average length " + (float)length / (float)moves);
            if(rounds>0)
                Debug.Log("Failure %: " + (float)failures / (float)rounds * 100);
        }
    }
}

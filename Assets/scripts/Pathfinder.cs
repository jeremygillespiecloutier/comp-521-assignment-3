﻿using System.Collections.Generic;
using UnityEngine;

/*
 * Jeremy Gillespie-Cloutier (260688666)
 * This class handles pathfinding.
 * I based myself on the approach described by david silver (with a few changes): http://www0.cs.ucl.ac.uk/staff/D.Silver/web/Applications_files/coop-path-AIWisdom.pdf
 */
public class Pathfinder : MonoBehaviour {

    int windowSize = 10; // The size of the planning window
    Level level;
    Menu menu;
    // The reservation table. For each timestep, it contains the list of cells reserved by each shopper
    // Eg. reservations[2][shopper1] would be the list of cells reserved at t=2 by shopper 1
    List<Dictionary<Shopper, List<Cell>>> reservations = new List<Dictionary<Shopper, List<Cell>>>();
    List<CellInfo> border = new List<CellInfo>(); // The open list (cells to be visited)
    List<List<Cell>> visited = new List<List<Cell>>(); // The closed list (cells already visited)
    int[,] distance; // Contains the shortes distance between any two vertices (ignoring other units). used for true distance heuristic

    void Start()
    {
        level = GameObject.Find("Level").GetComponent<Level>();
        menu = GameObject.Find("Canvas").GetComponent<Menu>();
        // Initialize the reservation table
        for(int i = 0; i < windowSize; i++)
            reservations.Add(new Dictionary<Shopper, List<Cell>>());
        for (int i = 0; i < windowSize; i++)
            foreach (Shopper shopper in level.shoppers)
                reservations[i][shopper] = new List<Cell>();
        calculateDistances();
    }

    void calculateDistances()
    {
        int index = 0;
        List<Cell> cells = new List<Cell>();
        foreach (Cell cell in level.cells)
            if (cell != null)
            {
                cell.index = index;
                cells.Add(cell);
                index++;
            }
        distance = new int[cells.Count, cells.Count];
        for(int i = 0; i < cells.Count; i++)
        {
            for (int j = 0; j < cells.Count; j++)
                distance[i,j] = 10000;
            distance[i, i] = 0;
            foreach (Cell cell in cells[i].neighbors)
                distance[i,cell.index] = 1;
        }
        for(int k=0;k<cells.Count;k++)
            for(int j=0;j<cells.Count;j++)
                for(int i = 0; i < cells.Count; i++)
                    if (distance[i,j] > distance[i,k] + distance[k,j])
                        distance[i,j] = distance[i,k] + distance[k,j];
    }

    // Stores information about cells (predecessor, heuristic, ...) for A*
    class CellInfo
    {
        public Cell cell; // The cell
        public float h; // The heuristic to goal
        public float g; // The cost so far
        public int index; // The time index at which the cell is visited
        public float f // f=g+h
        {
            get { return h + g; }
        }
        public CellInfo pred; // The predecessor of this cell
    }

    // Reserves a path from the shopper's current cell to the shopper's destination in the reservation table (space-time pathfinding).
    private void reserve(Shopper shopper)
    {
        int index = 0;
        border.Clear();
        visited.Clear();
        for (int i = 0; i < windowSize; i++)
            visited.Add(new List<Cell>());
        // Start by adding the shopper's current cell to the list of open cells
        CellInfo current = new CellInfo();
        current.cell = shopper.currentCell;
        current.g = 0;
        current.h = distance[shopper.destination.index, shopper.currentCell.index];
        current.index = index;
        border.Add(current);

        // As long as there are cells in the open list, keep looping
        while (border.Count!=0)
        {
            // Find the most promising cell in the open list
            current = null;
            foreach(CellInfo info in border)
            {
                if (current == null)
                    current = info;
                else if (current.f > info.f)
                    current = info;
            }
            // Remove it from open list and add it to closed list.
            border.Remove(current);
            visited[current.index].Add(current.cell);
            index = current.index; // Time index of current cell
            index += 1; // Next timestep (at which neigbhros of current cell will be visited)
            // If the next timestep is the same as the beginning one, no solution was found within the planning window. We will keep the partial path to current cell
            if (index == windowSize)
                break;
            // Expand the current cell
            foreach (Cell other in current.cell.neighbors)
            {
                // Check if the neighbor of current cell are reserved at next timestep
                bool blocked = false;
                foreach(Shopper shopper2 in reservations[index].Keys)
                    if (shopper != shopper2)
                        if (reservations[index][shopper2].Contains(other))
                            blocked = true;

                if (!blocked && !visited[index].Contains(other)) // If the neighbor is not reserved, add it to the open list
                {
                    CellInfo otherInfo = new CellInfo();
                    otherInfo.cell = other;
                    otherInfo.g = current.g + 1;
                    otherInfo.h = distance[shopper.destination.index, other.index];
                    otherInfo.pred = current;
                    otherInfo.index = index;
                    border.Add(otherInfo);
                }
            }
            // Also check if current cell is reserved at next timestep
            bool blocked2 = false;
            foreach (Shopper shopper2 in reservations[index].Keys)
            {
                if(shopper!=shopper2)
                    if (reservations[index][shopper2].Contains(current.cell))
                        blocked2 = true;
            }
            if (!blocked2) // If current cell is not reserved at next timestep, also add it (since we are allowed to stay in the same spot)
            {
                CellInfo currentCopy = new CellInfo();
                currentCopy.g = current.g + (current.cell==shopper.destination?0:1); // Staying in place still has a cost
                currentCopy.h = current.h;
                currentCopy.cell = current.cell;
                currentCopy.pred = current;
                currentCopy.index = index;
                border.Add(currentCopy);
            }
        }
        int nextIndex = current.index + 1; // The timestep after the last cell's timestep
        CellInfo last = current;
        // Reserve the path that was found by backtracking from last cell (current)
        while (current != null)
        {
            // Add cell to shopper's path, and reserve it at the time it is visited
            shopper.path.Insert(0, current.cell);
            reservations[current.index][shopper].Clear();
            reservations[current.index][shopper].Add(current.cell);
            // To avoid head to head collisions, also reserve the cell's predecessor at the cell's timestep (as described in David Silver's article)
            if (current.pred != null)
                reservations[current.index][shopper].Add(current.pred.cell);
            current = current.pred; // backtrack
        }
        // The first cell in the shopper's path is his current cell, but we don't need it since he is already there
        shopper.path.RemoveAt(0);
        generateHistory(shopper); // Add current path to the shopper's history of paths
    }

    // Generates the history of the last path calculated for the shopper (used to track statistics)
    void generateHistory(Shopper shopper)
    {
        PathHistory history = shopper.lastMove;
        if (history == null)
            history = new PathHistory();
        bool success = shopper.path[shopper.path.Count - 1] == shopper.destination;
        history.rounds += 1;
        history.length += shopper.path.Count;
        if (!success)
        {
            history.failures += 1;
            shopper.lastMove = history;
        }
        else
        {
            shopper.lastMove = null;
            menu.failures += history.failures;
            menu.length += history.length;
            menu.moves += 1;
            menu.rounds += history.rounds;
        }
    }

    public void pathFind()
    {
        foreach(Shopper shopper in level.shoppers)
        {
            for (int i = 0; i < windowSize; i++)
                reservations[i][shopper].Clear();
            reservations[0][shopper].Add(shopper.currentCell);
            shopper.path.Clear();
        }
        foreach (Shopper shopper in level.shoppers)
            reserve(shopper);
    }
}

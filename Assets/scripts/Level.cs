﻿using System.Collections.Generic;
using UnityEngine;

/*
 * Jeremy Gillespie-Cloutier (260688666)
 * This class creates and populates the mall. It also generates the grid-based graph that will be used for pathfinding.
 */
public class Level : MonoBehaviour {

    int stairLength = 3; // The length of the stairs
    int numShoppers = 5; //The number of shoppers in the simulation
    int numPlants = 4; // The number of obstacles in the simulation
    int storeSize = 3; // The width and height of each store. Since 9 ppl should fit, it is set to 3.
    int numStore = 6; // The number of stores on each platform (so 12 in total)
    int platformSize = 4; // The number of cells between the shops and the stairs
    public const float STEP_HEIGHT = 0.3f; // The elevation of each step of stairs
    public Cell[,] cells; // The grid-based representation of the mall
    float platformHeight; // The z-height of the upper platform (the lower one is at z=0)
    int stairDistance;
    public List<Shopper> shoppers = new List<Shopper>(); // The list of shoppers
    public List<Store> stores = new List<Store>(); // The list of stores

	void Awake () {
        platformHeight = (stairLength + 1) * STEP_HEIGHT; // Height of upper platform, accounts for number of stairs and elevation change of each stair.
        generateLevel();
	}

    void generateLevel()
    {
        cells = new Cell[(2*numStore-1)*storeSize, 2*(storeSize+platformSize)+stairLength];
        for(int i = 0; i < cells.GetLength(0); i++)
            for(int j = 0; j < storeSize+platformSize; j++)
            {
                Cell cell1=createCell(i, j, 0);
                Cell cell2=createCell(i, cells.GetLength(1) - 1 - j, stairLength+1);
                if (j < storeSize)
                {
                    cell1.tag = "store";
                    cell2.tag = "store";
                }
                else
                {
                    cell1.tag = "normal";
                    cell2.tag = "normal";
                }
            }

        stairDistance = cells.GetLength(0) / 5;
        for (int i = 0; i < stairLength; i++)
            for(int j=1;j <= 4; j += 1)
            {
                Cell cell=createCell(j * stairDistance, i + platformSize + storeSize, i + 1);
                cell.tag = "stairs";
                cell.blocking = true;
            }

        foreach(Cell cell in cells)
        {
            if (cell != null)
            {
                if (cell.x > 0 && cells[cell.x - 1, cell.y] != null)
                    cell.neighbors.Add(cells[cell.x - 1, cell.y]);
                if (cell.y > 0 && cells[cell.x, cell.y - 1] != null)
                    cell.neighbors.Add(cells[cell.x, cell.y - 1]);
                if (cell.x < cells.GetLength(0) - 1 && cells[cell.x + 1, cell.y] != null)
                    cell.neighbors.Add(cells[cell.x + 1, cell.y]);
                if (cell.y < cells.GetLength(1) - 1 && cells[cell.x, cell.y + 1] != null)
                    cell.neighbors.Add(cells[cell.x, cell.y + 1]);
            }
        }
        int mid = storeSize / 2;
        for(int i = 0; i < 2*(numStore)-1; i++)
        {
            Cell store1 = cells[i * storeSize + mid, mid];
            Cell store2 = cells[i * storeSize + mid, cells.GetLength(1) - 1 - mid];
            if (i % 2 == 0)
            {
                createStore(store1, false);
                createStore(store2, true);
            }
            else
            {
                createSeperation(store1, false);
                createSeperation(store2, true);
            }
        }
        populate();
    }

    void createSeperation(Cell center, bool platform)
    {
        int mid = storeSize / 2;
        for(int i = center.x - mid; i <= center.x + mid; i++)
            for(int j = center.y - mid; j <= center.y + mid; j++)
            {
                Cell cell = cells[i, j];
                foreach (Cell other in cell.neighbors)
                    if(other!=null)
                        other.neighbors.Remove(cell);
                cells[i, j] = null;
                GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                cube.transform.position = new Vector3(i, j, platform?-platformHeight-1:-1);
            }
    }

    void createStore(Cell center, bool platform)
    {
        int mid = storeSize / 2;
        Store store = new Store();
        for (int i = center.x - mid; i <= center.x + mid; i++)
            for (int j = center.y - mid; j <= center.y + mid; j++)
                store.cells.Add(cells[i, j]);
        stores.Add(store);
        float halfWidth = (float)storeSize / 2f;
        float left = center.x - halfWidth;
        float right = center.x + halfWidth;
        float top = center.y + halfWidth;
        float bot = center.y - halfWidth;
        createWall(left, bot, left, top, platform?platformHeight+1:1, false);
        createWall(right, bot, right, top, platform?platformHeight+1:1, false);
        if (platform)
        {
            createWall(left, top, right, top, platformHeight + 1, true);
            createWall(left, bot, center.x-0.5f, bot, platformHeight + 1, true);
            createWall(center.x+0.5f, bot, right, bot, platformHeight + 1, true);
            cells[center.x, center.y - mid].blocking = true;
        }
        else
        {
            createWall(left, bot, right, bot, 1, true);
            createWall(left, top, center.x - 0.5f, top, 1, true);
            createWall(center.x + 0.5f, top, right, top, 1, true);
            cells[center.x, center.y + mid].blocking = true;
        }
        for(int i=center.x-mid; i <= center.x + mid; i++)
        {
            Cell topCell = cells[i, center.y + mid];
            Cell botCell = cells[i, center.y - mid];
            if (i != center.x)
            {
                if (platform)
                {
                    Cell neighbor = cells[botCell.x, botCell.y - 1];
                    botCell.neighbors.Remove(neighbor);
                    neighbor.neighbors.Remove(botCell);
                }
                else
                {
                    Cell neighbor = cells[topCell.x, topCell.y + 1];
                    topCell.neighbors.Remove(neighbor);
                    neighbor.neighbors.Remove(topCell);
                }
            }
        }
    }

    void createWall(float x1, float y1, float x2, float y2, float z, bool horiz)
    {
        Vector3 center = new Vector3((x1 + x2) / 2, (y1 + y2) / 2, -z);
        float width = Mathf.Abs(x2 - x1);
        float height = Mathf.Abs(y2 - y1);
        float scaleX = width, scaleY = 0.05f;
        if (!horiz)
        {
            scaleX = scaleY;
            scaleY = height;
        }
        GameObject wall = GameObject.CreatePrimitive(PrimitiveType.Cube);
        wall.transform.position = center;
        wall.transform.localScale = new Vector3(scaleX, scaleY, 1);
    }

    void populate()
    {
        List<Cell> choices = new List<Cell>();
        foreach (Cell cell in cells)
            if (cell != null && !cell.blocking && ((cell.y >= storeSize && cell.y < storeSize + platformSize) || (cell.y >= (storeSize + platformSize + stairLength) && cell.y <= cells.GetLength(1) - 1 - storeSize)))
                choices.Add(cell);

        List<Vector3> plantsCreated = new List<Vector3>();
        for (int i = 0; i < numPlants; i++)
        {
            while (plantsCreated.Count < numPlants)
            {
                int index = Random.Range(0, choices.Count - 1);
                Cell cell = choices[index];
                choices.RemoveAt(index);
                bool valid = true;
                Vector3 pos = new Vector3(cell.x, cell.y, 0);
                foreach (Vector3 otherPos in plantsCreated)
                    valid &= (pos - otherPos).magnitude > 2f;
                if (valid)
                {
                    plantsCreated.Add(pos);
                    GameObject plant = (GameObject)Instantiate(Resources.Load("Plant"));
                    plant.transform.position = new Vector3(cell.x, cell.y, cell.z * -STEP_HEIGHT - 1);
                    foreach (Cell other in cell.neighbors)
                        if(other!=null)
                            other.neighbors.Remove(cell);
                    cells[cell.x, cell.y] = null;
                }
            }
        }
        choices.Clear();
        foreach (Cell cell in cells)
            if (cell != null)
                choices.Add(cell);
        for (int i = 0; i < numShoppers; i++)
        {
            int index = Random.Range(0, choices.Count - 1);
            Cell cell = choices[index];
            choices.RemoveAt(index);
            GameObject shopperObj=(GameObject)Instantiate(Resources.Load("Shopper"));
            shopperObj.transform.position = new Vector3(cell.x, cell.y, cell.z*-STEP_HEIGHT - 1);
            Shopper shopper = shopperObj.GetComponent<Shopper>();
            shopper.currentCell = cell;
            shopper.destination = cell;
            shoppers.Add(shopper);
            shopperObj.transform.GetChild(0).GetComponent<TextMesh>().text = (i + 1) + "";
        }
    }

    Cell createCell(int x, int y, int z)
    {
        Cell cell = new Cell(x,y,z);
        cells[x, y] = cell;
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.tag="Cell";
        cube.transform.position = new Vector3(x, y, z*-STEP_HEIGHT);
        return cell;
    }

}

public class Cell
{

    public int x, y, z, index;
    public List<Cell> neighbors = new List<Cell>();
    public bool blocking = false;
    public string tag;

    public Cell(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}

public class Store
{
    public List<Cell> cells=new List<Cell>();
}

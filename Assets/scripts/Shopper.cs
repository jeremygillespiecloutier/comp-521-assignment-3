﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Jeremy Gillespie-Cloutier (260688666)
 * This class contains information about shoppers
 */
public class Shopper : MonoBehaviour {

    public Cell destination; // The shopper's current destination
    public Cell currentCell; // The shopper's current position
    public List<Cell> path = new List<Cell>(); // The next cells to be visited by the shopper
    public PathHistory lastMove; // Statistics about the shopper's last move
}

/*
 * This class tracks statistics about a Shopper's last move (between the time he choses a new destination and reaches it)
 */
public class PathHistory
{
    public int rounds = 0, // The number of pathfinding rounds it took for the shopper to reach his destination
        failures = 0, // The number of rounds that failed. If it takes 3 rounds in total to reach the destination, then the first 2 were failures.
        length = 0; // The number of time steps it took to reach the destination
}
